---
title: Home
type: docs
---

<p style="text-align: center"><img style="width: 66%" src="/lab-logo.svg"></p>

# Home

The knowledge base is divided into a number of categories and sub-pages; start
exploring on the left-hand side to see what information is here. Is there
something that you think is missing?
[Drop a line to Prof. Pence.](mailto:charles@charlespence.net)
