---
title: Amenities
weight: 2
---

# Amenities

There are a variety of small amenities available in and around UCLouvain. I try
to keep a decent collection of them here.

- The philosophy library is found underneath the department offices, in the
  basement of Collège Mercier. Your UCLouvain ID card provides you access and
  check-out abilities.
  - To access library resources from off-campus, the easiest thing to do is
    start at the [library catalog page](https://bib.uclouvain.be/) and click the
    link for "Accès à distance" which should appear right below the user sign-on
    box.
  - Inter-library loans can be accomplished by talking to the librarians there.
    It costs a couple of euros, but it's 100% certain that we can pay for it;
    tell me and we'll get it billed to the right account.
- In the Institute mailroom there's a pretty decent espresso machine, a water
  kettle, and a nice cold-water dispenser. If the room is locked, use the code
  "11425A" on the access panel.
- I also have a kettle in my office, as well as nice coffee beans,
  filter-coffee-making products, and nice tea. (No espresso machine. Yet.) If
  you want any of those things, come by! I'm happy to chat over coffee or tea
  basically always.
- There are [showers available around
  campus](https://uclouvain.be/fr/decouvrir/douche.html) for those who would
  like to bike to the office or go for a run at lunch.
- Louvain-la-Neuve's weekly market is on Tuesdays. There's a number of fun
  things that visit. (I particularly like the coffee roaster, and the Moroccan
  place over on the side of Place de l'Université nearest the train station
  makes an excellent couscous.)
