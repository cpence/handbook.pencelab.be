---
title: Apartments
weight: 1
---

# Apartments

The universal source for apartment hunting in Belgium is a site called
[Immoweb.](https://www.immoweb.be/) It's available in Dutch, French, and English
(though not every listing will be translated by the real estate agent).
Apartment rentals in Belgium also very often happen with the aid of a real
estate agent – for the Americans reading this, this is completely normal and
shouldn't surprise you. In general, then, you'll be working with an agent to set
up visiting times for apartments. I was able to find something in about two
weeks of maniacal searching.

Which neighborhoods should you look in? Here's some advice. If anyone has
experience to add to any of these sections, let me know!

## Outside Brussels

### Louvain-la-Neuve

No one affiliated with the lab yet has experience using it, but the University
does operate some [housing for students and
researchers.](https://uclouvain.be/en/study/accomodation) Please do get in touch
if you try it!

Not very many people between the ages of 25 and 75 live in Louvain-la-Neuve, but
it does happen. It's a quiet town, and a great place to get lots of work done. I
also hear that the schools are nice, and so it could be attractive to anyone
with children. Unfortunately, I know little about its neighborhood structure.
The Bruyères neighborhood is the one "organic" neighborhood (i.e., it wasn't
centrally planned along with the rest of the city in the '60s), and I've stayed
there briefly once; it seemed quiet and rather nice, though I'm told that
rentals can be few and far between.

### Leuven

If you speak Dutch (local government can sometimes be very reluctant to operate
in English, and _will not_ operate in French), Leuven can be a fantastic choice.
I love the city – there's a lot going on, great restaurants, nice schools, and
it's also fairly quiet if you're not in the heart of a student area.

### Small-Town Wallonia

I know of several people who work for UCLouvain and live in a variety of the
small towns in Brabant-Wallon (Court-Saint-Étienne, Genval, La Hulpe, Rixensart,
etc.). If you speak very good French, and the more rural life appeals, you
should definitely check them out.

## Brussels

The rest of the following are the various communes/neighborhoods that make up
Brussels. My guiding principle here is the assumption that you want to take the
train to Louvain-la-Neuve. This means you'll want to be toward the southeast
side of the city, as being anywhere else will add around a full hour to your
round-trip daily commute. The upside is that there are some really cool
neighborhoods on this side of town. The downside is that many of them can be
expensive.

### Etterbeek

Etterbeek is probably the most classic location for UCLouvain-affiliated folks
to live. It's a quiet commune, has loads of housing (though can be pricey), some
really fantastic schools, access to great parks and public spaces (such as the
Parc du Cinquantenaire and the several museums that it holds), and is
excellently metro, tram, and bus connected to the rest of Brussels. You should
be able to get quickly to Schuman or Luxembourg stations via the bus and/or
metro if you are careful about apartment choice.

### Ixelles

I live in Ixelles, so it's become my favorite commune. The southern part, near
the ULB and VUB campuses, is very student-focused; the center part, near Place
Flagey is more of a mix of night-life and settled families; and the top part,
near Place Fernand Cocq, is even quieter, but has fantastic access to the city
center. The entire commune is quick on the bus to the middle of town, and if you
plan carefully, you should be able to quickly get to either Schuman, Luxembourg,
or Etterbeek train stations, all of which are very fast to LLN.

The Châtelain neighborhood on the western side of the commune is one of the
highest-regarded neighborhoods in the city, especially for young people with
families, but it brings you a touch farther away from good transit to LLN.

### Watermael/Boitsfort, Woluwe-Saint-Pierre, Woluwe-Saint-Lambert

A bit further out, thus farther from the city center, but quieter and with more
space available. I've heard that it can be _quite_ expensive, but otherwise I
know very little.

### Saint-Gilles

Saint-Gilles is probably the coolest commune in Brussels (it's the Brooklyn of
Brussels). I go all the time for the fantastic bars and restaurants. As a rule,
I have heard it said that the farther you go north toward the Porte de Hal, the
shadier the neighborhood gets, but this is a pretty relative notion of shady;
it's always felt safe to me.

The only trouble is, it's very badly positioned with respect to transit to LLN.
You can get quickly to Bruxelles-Midi, but that's the single farthest station
away from LLN, and the only trams through Saint-Gilles run either back north
into the center of the city, or require you to go all the way through Ixelles to
get close to Etterbeek station. If you don't mind a long commute and want to
live somewhere funky, it's the spot.

### Others

There are something like 13 other communes in Brussels, and I don't have
information on most of them. If you learn anything, or happen to be madly in
love with one of them, let me know!
