---
title: Directions
weight: 3
---

# Directions

Getting to the Institute is not exactly easy.

The ISP is located in Collège Mercier, which is part of a larger building also
sometimes labeled Auditoires Socrate, after the large set of teaching rooms it
also contains. It sits on a square called Place du Cardinal Mercier.

First and foremost, if you're looking for the ISP on Google Maps, you _can_
search for "Institut supérieur de philosophie," and the pin _will_ be right, but
it's located where we really physically are in the building, not at the entrance
to the building that you're actually trying to get to. If you search for the
"UCLouvain Faculté de psychologie," that pin will appear right at the entrance
that you're going to be trying to walk into.

## By Train

If you're coming from the train, you'll first want to climb the stairs and pass
straight through the building in front of you (which is both the train station
and a small commercial center). Exit the far side and turn right. In front of
you, you'll see an Exki café. Go down to it and turn left. That will put you in
L'Esplanade, a long street of cafés and stores. Walk all the way down it (past
the Paul café about halfway down) and you'll arrive at the Grand Place of
Louvain-la-Neuve. Right in front of you will be the large theology building,
with a clock in the top center.

You should now exit the Grand Place just to the right of the theology building,
out of the "back right" corner of the Grand Place with respect to where you
enter. As you get close to it, you should see a sign in that corner pointing
toward Collège Mercier. You'll go down a little street and into a (nearly)
dead-end square. One of the buildings in front of you and a bit to the right
will have a big "Auditoires Socrate" sign on it. Go in the left-hand door on
that building (it has a sign on it for Collège Mercier, and it also mentions
Institut supérieur de philosophie). You'll be in a stairwell. Head up a floor
and you'll be at the Institute's main offices.

If you're here for a talk, you're probably looking for the Salle Jean Ladrière,
which you can find by following the sign on the red door just to the left of the
elevator. If you're looking for Pence's office, go back to the stairs, go up one
more floor, and go through the door down the hallway to b.214.

## By Car

The easiest way to drive in to campus is to enter via Boulevard André Oleffe
(see the map below.) You'll pass around a roundabout, through a small tunnel,
and then see the entrance to the parking lot "Grand Place." (Unfortunately, this
is a paid lot; the nearest free parking is _quite_ far away.) From there, park
your car and exit the lot. You'll pop up in the Grand Place. To orient yourself,
look for the large brasserie that's just called "Grand Place" along one whole
side of the square, and the theology building (with a clock on the front) on the
opposite side of the square. At that point, you can scroll up and pick up the
train directions, starting at "You should now exit...."

### Map

![Map courtesy OpenStreetMap](map.png)

This map indicates the basic driving directions (red arrow), the location of the
Grand Place (pale red square), and the Institute (purple star). The train
station is off the map to the right -- when walking from the train, you'd be
coming down the top-right road that enters into the Grand Place (past the purple
grocery-cart icon).
