---
title: Getting Help
weight: 2
---

# Getting Help

Having trouble? Look here for a list of resources that can help.

## Administrative Concerns

First, there are a few places you should always go for specific administrative
problems:

- **The SGSI Service Desk.** Anything regarding computers, telephones, internet
  access, e-mail, and so on is serviced by the Service Desk, which can be
  reached by calling 7-8282, or by sending an e-mail to <8282@uclouvain.be>.
  Almost any other way of getting help with our IT systems will wind up creating
  a ticket there eventually, so it's better to just start at the source; it'll
  save you time later.
- **The ISP accountant (or CLC, currently Eric Legrand).** Anything concerning
  procurement or reimbursements (_other_ than computers, which are handled by
  SGSI), as well as basic office supplies questions, are handled by the ISP's
  accountant, Eric. You can send him a message at <clc-isp@uclouvain.be>.

For other, general problems, almost all administrative problems can be solved
either by, in escalating order of importance:

1. **The secretary of the ISP (currently _in flux_).** If you've got something
   straightforward, it's always a good idea to stop by the secretary's office on
   the first floor of the ISP. If you've got basic questions (like sending mail,
   etc.), someone there will help sort them out.
2. **The administrative coordinator (or CAI) of the ISP (currently _missing_).**
   The CAI is the next level up, who can resolve nearly any administrative
   concern, or put you in direct touch with the person who can. They also have
   access to keys for nearly every room in the building.
3. **The president of the ISP (currently Jean-Michel Counet).** The Institute
   President is, formally, all of our boss as regards research concerns, and
   hence is your first point of contact with the proper administration if you
   have need of escalating something serious.

## Research Concerns

We wouldn't have a group like we do if we weren't dedicated to trying to make
every member as productive and happy as you can be. So if one lab member has
research troubles, that should bother everybody! Most importantly, don't
hesitate to reach out for help when you get stuck. The structure of monthly
stand-up meetings is designed to give you a place to air grievances and keep you
from being held up by something for too long. Don't hesitate to mention stuff in
and around reading group, as well, which is even more frequent.

If you have particular, concrete issues about material, references, books, etc.,
feel free to drop a line either to Prof. Pence or to anybody who works in the
ISP library (one floor beneath ground level, underneath the ISP itself). The
group almost always has some amount of funding available for purchasing books
and other materials.

## Ethical Concerns

If you have a research ethics concern, please first raise it with Prof. Pence at
the first opportunity, or, if you do not feel comfortable doing so, please get
in touch with ADRE (the research office). Their central contact e-mail for
research ethics concerns is <ethics@uclouvain.be>.

## Teaching Concerns

If you're teaching in EFIL (the philosophy faculty), your first point of contact
will either be the Head of School (currently Sylvain Camilleri) or the FIAL
coordinator in EFIL (currently Marie-France Delcourt). The Head of School has
more authority to handle serious issues (questions about course hours, payment,
course structures, etc.); the FIAL coordinator can handle more nuts-and-bolts
problems (with grades, particular students, timing of classes, etc.).

## In General

Don't be afraid to ask questions! We're an interdisciplinary group, and so we're
all always working on the edge of our comfort zone. That's okay! We can't do the
kind of work we want otherwise. But it also means we've got to all be quite
willing to admit when we need help.
