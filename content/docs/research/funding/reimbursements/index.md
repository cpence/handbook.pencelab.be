---
title: Reimbursements
weight: 2
---

# Reimbursements

What exactly can be reimbursed, to whom, and when, is always a bit confusing.
Ask if there's a question that's not answered here!

## What Funding Is Available?

In general, reimbursement comes from three main sources. First, almost every
project grant that I write includes three categories of reimbursements: travel
funding for the relevant lab members; funds to buy technology, website hosting,
and so on; and what our grants often call "documentation costs," which can cover
both open-access publication fees as well as the purchase of books. For this
kind of funding, talk to me first about what you'd like to do, then prepare your
reimbursement form and bring it to me (see below) and I'll take care of it.

Second, many postdoc or doctoral grants (such as those from the FNRS or the FSR)
will often come with a "budget de fonctionnement" which will cover, in essence,
_any_ research-related costs (within the categories spelled out below) that the
researcher would like to bill to them. You should already know (or you can ask
me for) the account number for that budget, if you have one; you can then freely
submit reimbursements to it whenever you would like (see below).

One overall caveat for either of these first two cases: if you're funded (either
personally or project-wise) by the FNRS, they tend to be pretty strict about
reimbursements. You'll need to exhibit very clear research-based reasons for
your expenses, and you can only usually do very simple things with, e.g.,
travel.

Finally, the Institut supèrieur de philosophie (ISP) has its own "mobility"
funding for travel to conferences, to which you can apply. (FIXME: I don't yet
know how this funding works! I'll get more info when I can.)

## What Can Be Reimbursed?

### How Much Money?

How to do reimbursement depends first on how much the item costs. You should
include any expected costs in your estimation -- like consumables, maintenance
contracts, service contracts, etc. You should also estimate the cost over a
reasonable period of time, such as three to four years, to not be accused of
underestimating your costs.

- **Between €1 and €8,500:** No need to talk to the purchasing service, as long
  as these are not recurrent expenses. If you are purchasing something for which
  there exists a competitive market, you should obtain three quotes from
  different providers, and send them to me so that I can keep them in case the
  grant expenditures are audited.

- **Between €8,500 and €29,999:** You must contact the purchasing service
  ("Service des Achats"). They will draw up a purchasing dossier, you'll get
  three quotes from competitors, and then the service will help you negotiate
  the quotes with each of the providers. You'll then be obligated to take the
  lowest quote, and the purchase will need a purchase order prepared by the
  accounting service.

- **More than that:** What?

### For What?

**N.B.:** If you’re funding something from an FNRS grant, there is a detailed
[guide from the FNRS concerning what can be financed.](fnrs-guide.pdf)

- Books: Can always be reimbursed. There is a procedure (though I don't know
  exactly how it works) in place with the FNAC in Louvain-la-Neuve, to which you
  can take a "vignette" and have them bill the charges directly to your
  university account. (Note: do _not_ do this for computer equipment; see
  below.)

- Cloud hosting: Unlike other IT related expenses, cloud hosting and other kinds
  of managed IT solutions do not need to be passed through SGSI and can be
  directly reimbursed.

- OA publication fees: If we have the budget for it, open access publication
  fees may be reimbursed from the operating budget of any FNRS grant, up to
  €750.

- Travel: [See the dedicated page about
  travel.]({{<relref "../travel/index.md">}})

- Hotels for visiting guests: UCLouvain has a contract negotiated with the
  Martin's and Ibis hotels in LLN, as well as a few other hotels in Brussels,
  Wavre, and Mons. The easiest thing to do here is have the Institute staff
  arrange that inbound travel for you.

- Office supplies, etc.: Send an order to the Institute's accounting person, who
  can put together the purchase order for you. There's actually quite a few
  restrictions about who you can buy from and what you can buy, more than is
  reasonable for us to keep up with.

## What Can't Be Reimbursed?

### Computers, Software, Peripherals, etc.

The procedure for anything computer-related is complex. The following list of
goods _must_ be purchased through the SGSI, the local IT service (any efforts at
submitting a reimbursement for them will be summarily denied):

1. Office and laptop computers (which must be Dell or Apple)
2. Printers
3. Software
4. Servers
5. Small peripherals

There are separate procedures for the purchase of each of these types of goods,
and different people at SGSI that you need to contact. To learn more about it,
you can try [clicking this
link](https://intranet.uclouvain.be/fr/myucl/administrations/adfi/informatique-et-bureautique.html),
or if that doesn't work, go to the UCLouvain intranet, to the "Administration
des finances (ADFI)", then follow: Recettes et Dépenses, En pratique, Achats,
Liste des produits, Informatique et bureautique.

In normal cases, it will be difficult to fund the purchase of an iPad or similar
tablet, given some bureaucratic rules that are currently in effect. If you need
one, let me know.

## Processing Reimbursements

Once you've bought something -- unless it was by purchase order or through the
travel agency -- we work on an after-the-fact reimbursement system. Thankfully,
getting reimbursed is relatively easy. There are two ways to do it, depending on
whether you're comfortable with the online form or would like to submit paper
receipts. (Some grants may _require_ paper receipts. I will have told you about
this in advance, if so.)

### Online Form

To create an online reimbursement, you need to visit your UCLouvain "bureau
virtuel" (at [https://intranet.uclouvain.be/](https://intranet.uclouvain.be/)).
You will probably already have, pre-configured, a box of links labeled
"Finance." If you don't, click "Personnaliser mon bureau virtuel," then "Ajouter
un widget." In the list of widgets, click "SAP," then make sure the "Type de
liens" is set to "Finance."

Now, if you scroll down in the "Finance" list, you should see a list titled
"Note de frais — Créer." That will take you to the expense-reporting section.
Fill in the basics: select yourself under "Bénéficiaire," and fill in a quick
description explaining what the reimbursement is intended to cover. Under
"Approbateur," select me.

If your grant came with a personal account (a budget de fonctionnement) for your
expenses, you may have been given access to debit it directly. If so, you will
see a line just below "CLC/Gestionnaire" which reads "Compte." If you have that,
click it, and select your account. If you don't, don't worry; that just means
that I will need to select the appropriate account for you, which I'll do after
you submit the report. Under “CLC/Gestionnaire,” select the current ISP CLC –
right now, Eric Legrand.

For each expense, you can now click "Ajouter une dépense," and fill in the
details. The basics should be easy: a pre-defined type under "Nature," then
date, quick and long descriptions, currency and amount.

Finally, with all the expenses entered, you need to upload a PDF containing all
of your receipts in the "Annexe" field. **Important:** If something is merely a
reservation document (this applies especially to airplane e-tickets, for
instance!), which doesn't have any proof of payment involved (i.e., it doesn't
show a zero-euro balance at the bottom), then you **must** also upload an
excerpt from your credit card or bank statement showing that the expense was in
fact paid, and not just reserved.

### Third Parties

For reimbursements to third parties (anyone who doesn't have a bank account
registered with the university), you have no choice but to process [the paper
form,](external-28.09.2018.xlsx) which can also be found on the intranet in the
same place as the other paper form above. Fill in all the details and e-mail it
to the [e-mail address for the ISP CLC.](clc-isp@uclouvain.be)

If you need more information about how to fill in the external person’s bank
account details, you can find that [in this PDF guide.](external-info.pdf)
