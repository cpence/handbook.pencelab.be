---
title: Crediting Grants
weight: 1
---

# Crediting Grants

We're funded by a variety of sources, and should go out of our way to make sure
that we credit them.

- **EC Grant:** for any work on the ethics of organoids or similar bioethics
  work from 2021--2024:
  - "This project has received funding from the European Union's Horizon 2020
    research and innovation programme under grant agreement No. 101006012".
  - You are also expected to put the EU flag on literally anything where it
    would be reasonable to do so.
- **Templeton Grant:** for work on the digital cultural evolution project:
  - “This work was supported by the John Templeton Foundation under grant no.
    62377.”
- **PDR Grant:** for any work on biodiversity and species from 2021--2024:
  - "This work was supported by the Fonds de la Recherche Scientifique - FNRS
    under grant no. T.0177.21."
- **MIS Grant:** for any work on digital philosophy of science from 2019--2021:
  - "This work was supported by the Fonds de la Recherche Scientifique - FNRS
    under grant no. F.4526.19."
- **WBI-Québec Grant:** for any work collaborating with people at UQaM between
  2019--2021:
  - "This work was supported by the Commission mixte permanente
    Wallonie-Bruxelles Québec under grant no. RECH-INNO-05."
- **NSF Grant:** for any work on history of biology from 2018--2019:
  - "This work was supported by the US National Science Foundation under HPS
    Scholars Award #1826784."
