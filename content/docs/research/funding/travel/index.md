---
title: Travel
weight: 3
---

# Travel Funding

(This is a skeletal page, still under development.)

## Booking Travel

This is a highly regulated process, because the university has obtained
exclusive contracts with travel companies. You have two choices for booking your
travel.

**Important:** In either case, you should keep your e-tickets, boarding passes,
train tickets, hotel vouchers and bills, etc., and submit them all to the
Institute's accountant for possible future auditing purposes.

### Use Carson Wagonlit Travel

This is the preferred option, because it's easier, and also saves you from
having to advance the money for the tickets yourself.

Write the travel agency at <team2fsc.bru.be@contactcwt.com>, including (1) the
name of the traveler, (2) the destination and times requested, (3) the account
number (including all of the optional account number parts). As always, if you
don't have an account number, contact me.

Confirm your itinerary when you receive it, and the payment will be taken care
of automatically by the university (you don't have to pay in advance!).

This will automatically get you corporate-rate Thalys, Eurostar, and
Lufthansa-group tickets.

### Buy Tickets Yourself

You are welcome to book your own travel, but you **must** get three quotes for
three different itineraries and take the lowest available price. This
theoretically applies to hotels as well, but for hotels it's much easier to
justify not offering three competing quotes (you can explain that you selected
on the basis of location, being the official conference hotel, etc.).

## Reporting Absences

For insurance and bookkeeping purposes, you _must_ report your travel to the
Institute ahead of time. This ensures that your insurance is correctly handled
(in the event that you get sick or injured while abroad, if this occurs during a
trip for work, it's the university that will be responsible for paying for your
expenses), and that your being gone has been registered with HR.

For graduate students and postdocs funded by grants, you do this by filling in
[this PDF form](travel-boursier.pdf) and sending it to the ISP's administrator,
Caroline Dutry ([cai-isp@uclouvain.be](mailto:cai-isp@uclouvain.be)). (For
academics, [this form](travel-aca.pdf) should be used instead.) You must send in
this form **one month in advance** of the planned travel.
