---
title: Conferences
weight: 1
---

# Conferences

There's a number of conferences that lab members often or usually go to. Here's
a brief summary of each. They're sort of in an order, but it's not really meant
to be very meaningful. You should read the whole thing and consider going to all
of them, plus more that aren't on the list!

Also, if you attend a particularly exciting meeting and think that the group
would benefit from hearing a brief summary of some of the coolest talks that you
went to, let me know and we'll schedule one!

It's really important, when you attend conferences, to push yourself to meet new
people and build networks. I know that lots of philosophers are card-carrying
introverts, but it's really important that you do what you can to see what other
things people are working on, extend your contacts (especially internationally),
and keep your finger on the pulse of the broader field/discipline. If you're not
doing that, there's no reason to go to a conference in the first place!

- **ISHPSSB,** or the International Society for the History, Philosophy, and
  Social Studies of Biology: If you're the kind of person who wants to work on
  these sorts of topics, this will probably be your single Must Attend meeting.
  It occurs every other summer, in odd-numbered years. You can bet that if
  you're on a project grant during an ISH, there'll be funding to attend.
  Basically all papers are accepted (so that people can get travel funding!), as
  long as they have something to do with history or philosophy or sociology of
  biology.

- **EPSA,** or the European Philosophy of Science Association: The finest
  general philosophy of science meeting in Europe. I have to say that EPSA talks
  have consistently been among some of the best that I've been to in my time as
  a philosopher of science. Strongly recommended for any papers that range from
  fairly technical philosophy of biology to general philosophy of science.

- **PSA,** or the Philosophy of Science Association: The US association for the
  philosophy of science, which holds excellent, though very large meetings. Can
  often be quite difficult to get into (there's a high rejection rate for
  submitted papers), but is a good time, and very useful for networking. It'll
  accept anything from pretty technical work in philosophy of biology to very
  general work in philosophy of science or metaphysics of science.

- **HSS,** or the History of Science Society: The international association for
  general history of science. If you're more historically inclined, I strongly
  recommend it. It's going to really require a _history_ talk, and is often
  allergic to the kind of internalist/theory-focused history that HPS people do,
  but it's worth it.

- **&HPS,** or the Integrated History and Philosophy of Science meeting: This is
  an _amazing_ place to do truly integrated HPS work, if that's the kind of
  thing you're up to. Their Athens meeting remains perhaps my favorite
  conference I've ever attended. Also hard to get into, because _all_ the talks
  are long-format, and people take them very seriously.

- **SHESVie,** or the Société d'Histoire et d'Épistemologie des Sciences de la
  Vie: This is the French-language version of ISHPSSB, and is an excellent
  meeting to attend if you're comfortable presenting in French. I don't yet have
  a great read on the kind of scope that they accept, however.

- Three more conferences that I know aren't on the list are the SPS meeting (the
  general francophone society for the philosophy of science), and the BSPS and
  BSHS meetings in England. I haven't been to the BS\*S meetings at all yet, and
  I'm going to my first SPS in 2021. I'll update this when I can!
