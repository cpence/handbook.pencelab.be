---
title: Publicity
weight: 3
---

# Publicity

There's a number of places that I strongly encourage (and, in one case, require)
everyone in the lab to deposit their new publications after they appear. Here's
information on how to do that.

For those who aren't familiar, when I refer to a "preprint" version, I'm
speaking of the last version of the article that you submitted to the journal,
_prior_ to the final typesetting and pagination (so, your TeX or Word document
source). You should edit this preprint version to include a statement saying
that the official version of the paper is the published one, in whichever
journal, and that only the published version should be cited. (After all, it may
change in copy-editing and typesetting.)

Once you have a new publication uploaded at one or more of these sites, please
collate all the links and email them to me (Pence). I will update the lab's
central database of research and get them online, as well as publicized through
our social media channels.

- **DIAL:** preprint version (absolutely required)

  [DIAL](https://dial.uclouvain.be/) is the institutional archive of UCLouvain.
  Depositing articles within it is a mandatory condition of almost every grant
  funding opportunity that any of us would ever receive. They request that you
  deposit a preprint version of the paper.

- **PhilSci-Archive:** preprint version (strongly recommended)

  For papers in philosophy of science or integrated HPS, I strongly recommend
  that you upload a version to
  [PhilSci-Archive,](http://philsci-archive.pitt.edu/) the disciplinary
  repository for the philosophy of science. This site also requires preprint
  versions.

- **PhilPapers:** preprint version (recommended)

  For papers in philosophy (and perhaps in history of science, depending on
  journal), the philosophy indexing website
  [PhilPapers](https://philpapers.org/) will likely index the article shortly
  after it appears in the official journal. It's recommended to also go to the
  entry for your page yourself, both to add it to your PhilPapers author
  profile, and to upload a preprint copy into the archive that they
  independently run.

- **Lab Archive:** final published version (recommended)

  We run a locally-hosted archive of lab papers that will be downloadable
  directly from the publications page. Usually, I host the final, published
  copies of the papers here. (I am aware that, strictly speaking, this is not
  permitted by every publisher. I have yet to receive any cease-and-desist
  letters about it.)

- **figshare:** datasets \(very strongly recommended, if needed\)

  The lab is _strongly_ committed as a group to the principles of open and
  reproducible datasets. The data repository [figshare](https://figshare.com/)
  is the preferred place to deposit your data – and not just your data, but a
  full package of data, scripts, and instructions that would allow any
  interested researcher to reproduce your results. This package will then be
  given a DOI, and become citeable in future research.

- **Academia.edu, ResearchGate:** _strongly discouraged_

  Given that both of these companies intend to profit off of indexing and
  searching academic research outputs in order to sell advertising, while just
  providing resources that the above archives also already provide for free. I
  recommend that you avoid them.
