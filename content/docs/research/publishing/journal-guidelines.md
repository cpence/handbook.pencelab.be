---
title: Journal Guidelines
weight: 2
---

# Journal Guidelines

There are a vast number of journals in which our lab's work has appeared and
will appear. Here's a rough set of guidelines, with some brief notes about what
kinds of things (in my experience) have best fit in different places. I have
_almost certainly_ left venues off of this list; get in touch to make me add
more!

- **Pure philosophy of science**

  - _British Journal for the Philosophy of Science (BJPS)_ — incredibly high
    quality papers, on a vast array of topics, but usually requiring at least
    some connection to more general issues in philosophy of science or
    philosophy as a whole for publication.
  - _Philosophy of Science_ — very similar to BJPS, though with perhaps a
    slightly higher tolerance for engagement with science in practice. I believe
    this focus is in the process of changing, as well.
  - _Synthese_ and _Erkenntnis_ — see above; often happy to publish works in the
    philosopy of biology, especially if there's angles to more general problems
    or formal treatments.
  - _History of Philosophy of Science (HOPOS)_ — if you're working on the
    history of PoS, this is a fantastic place; they do notably interpret
    philosophy of science quite broadly, broadly enough to include Darwin and
    similar philosophically inclined scientists, for instance.
  - _European Journal for Philosophy of Science (EJPS)_ — still a very new
    journal, so I'm not quite sure what their scope is going to wind up being
    (e.g., I've yet to submit any really integrated HPSy stuff to them, so I
    don't know how they'll react). But it's getting really high quality papers
    and being widely read, so I recommend it.

- **Philosophy of science and (biological) practice**

  - _Philosophy, Theory, and Practice in Biology (PTPBio)_ — I'm a co-editor
    here, so definitely biased, but this is a great journal for articles with a
    genuine engagement with practicing biology (and all open-access, without
    author fees!). Feel free to submit here, I will make sure that I am not
    involved with the editorial process and there will be no conflict of
    interest. (I will not submit my own papers here, however, while serving as
    editor.)
  - _Biology & Philosophy_ — has recently changed editors and broadened a bit in
    focus, now accepting basically anything that engages with biologists. Here
    and for PTPBio, you may well get a biologist as a reviewer, so think about
    that as you're considering submitting there.
  - _Biological Theory_ and _Acta Biotheoretica_ — in both cases, very good
    journals for _very_ technical work that engages very seriously with the
    biology.

- **Integrated HPBio**

  - _Studies in History and Philosophy of Biological and Biomedical Sciences
    ("Studies C"/SHPSC)_ — really remains the flagship journal for properly
    integrated stuff, though you will find people who have a weirdly low opinion
    of its quality (in part because of the unfortunate "C" in its full name).
  - _History and Philosophy of the Life Sciences (HPLS)_ — a nice outlet, the
    quality of which has risen quite a bit in recent years. Quite similar
    otherwise to SHPSC.
  - _Studies in History and Philosophy of Science ("Studies A"/SHPSA/SHPS)_ —
    actually a completely different and non-overlapping editorial process to
    SHPSC. If you have stuff that is less purely biology-focused, or you want to
    use integrated work to draw a more general philosophy of science point, this
    is the spot.

- **Digital Humanities**

  - Note that all of the above philosophy of science journals have been
    increasingly willing to publish digital analyses in recent years (I believe
    we've now seen things get printed at _HOPOS_, _Phil Sci,_ and _B&P_).
  - Otherwise, if something is too strange or technical for the philosophy
    journals, I've just placed it at _PLoS ONE._ The page charges are expensive,
    but distribution is wide and open-access is nice.

- **History of Biology**

  - _Journal of the History of Biology (JHB)_ — straightforwardly the field's
    flagship, if you have some very high quality historical research to print.

- **General Philosophy Journals**
  - Most general philosophy journals don't want much of anything to do with
    philosophy of science, especially anything that's really practice-oriented.
    That said, _Australasian Journal of Philosophy_ has been printing more stuff
    of late, and if you can be one of the people to get in the one philosophy of
    science paper per year or two that they print in _Journal of Philosophy,_
    then more power to you. Expect long review times.
