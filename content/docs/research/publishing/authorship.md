---
title: Authorship
weight: 1
---

# Authorship

Authorship guidelines in philosophy are an absolute nightmare, as the field
still hasn't really caught up to co-authored publications. Here's a few thoughts
about some guidelines.

## Single-Authored Publications

First off, I should emphasize that I am _very strongly_ in favor of all lab
members continuing to produce single-authored publications while they're here.
As long as philosophers are confused about coauthoring, there will always be
people who are convinced that a paper coauthored between two people is half the
work, and so a healthy armor of single-authored pieces can be crucial for job
market prospects.

I should add, however, that I'm happy to provide comments and even work with you
on single-authored projects. Just make sure to say up-front that you don't want
a project to turn into a coauthorship; that's an entirely legitimate personal
choice that you shouldn't feel bad about making!

## Coauthored Publications

I tend to support pure alphabetical order for authorship, since there's no other
standard in philosophy. That said, since multi-authored publications often spend
the rest of their lives as "X et al.," I'm also in favor of giving the "lead"
author the first authorship position, when that's a thing that makes sense.

I should also reinforce that I do _not_ believe in adding me, or anyone else,
for that matter, to papers to which we did not contribute, for "political"
reasons. Mercifully, authorship in philosophy has continued to be held to a
fairly high standard – in the sense that you have to actually contribute to be
an author – and I don't want to be part of degrading that in any way.
