---
title: Data Storage
weight: 1
---

# Data Storage

Obviously, the lab traffics in a variety of large to very-large datasets. To
that end, we have a variety of storage tools that researchers can use for
projects.

## Lab NAS

There's a lab NAS in Prof. Pence's office. Unfortunately, we're still working on
the ability to connect to the NAS from elsewhere on campus. Apparently it's
relatively frowned-upon to have private servers at UCLouvain, but there's not
really any infrastructure to get multiple terabytes of storage otherwise. So for
now, if you need to deal with something _really_ big (at the moment, primarily
just the evoText full-text archives), come by the office. There's a small
pirate-radio network that you can connect to, and that will give you fileserver
access; ask me about it if you need it.

## rsync.net

The contents of the lab NAS are mirrored to the offsite backup service provided
by [rsync.net](https://rsync.net). This service is expensive, but provides us
our only (and very important) off-site backup service.

## UCLouvain Storage

If you really want to have UCLouvain host your storage for you, there is mass
storage available from the [Center for High-Performance Computing and Mass
Storage \(CISM\).](https://uclouvain.be/en/research/cism) There is a fee
associated with their services, so talk with me and/or them before setting
anything up for the first time.

There are rumors about an old group storage system via the Windows Active Domain
controller (which is called 'oasis'). I can however only find very old pages
about this on the UCLouvain website, so I'm led to believe that the system was
shut down once they realized that everybody was using personal Dropbox, Google
Drive, etc. accounts.
