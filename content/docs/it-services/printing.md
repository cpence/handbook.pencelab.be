---
title: Printing
weight: 2
---

# Printing

There are three printers that you can easily access.

## Brutus

This is the color printer (not the integrated printer-copier machine) on the
first floor of the Institute, housed in the small room just in front of the
entrance to the Institute offices. The code for this door is 1142.

It is an HP Color LaserJet Enterprise M552. The URL for this printer is:

```text
smb://OASIS\<your UCLouvain login ID>:<your UCLouvain password>@ucl-prn02.oasis.uclouvain.be/MERCIER_BRUTUS
```

## Lotus

This is the integrated scanner-printer-copier that's also in the room with
Brutus. It remains unclear to me whether or not you can print to this without
having some kind of permissions already set up on your account. I almost always
print to the other two printers instead.

It's a Ricoh IM 2500. The URL for this printer is:

```text
smb://OASIS\<your UCLouvain login ID>:<your UCLouvain password>@ucl-prn02.oasis.uclouvain.be/MERCIER_LOTUS
```

## Popeye

This black and white printer/scanner/copier is in a small printer room on the
2nd floor, around the corner from Pence's office in room B.237. The code for the
access door is 441955.

It is a Ricoh MP 2555. The URL for this printer is:

```text
smb://OASIS\<your UCLouvain login ID>:<your UCLouvain password>@ucl-prn02.oasis.uclouvain.be/MERCIER_POPEYE
```

For all three printers, note that if you're going to add them manually, your
username needs to be `OASIS\username` (to look like you're coming from the
Windows domain).
