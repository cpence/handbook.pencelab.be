---
title: Computers
weight: 3
---

# Computers

Most incoming PhD and postdoc fellowships are budgeted for the purchase of a
computer. To obtain it, you'll need to talk to UCLouvain's informatics services,
called SGSI. These are first split up geographically, and then by "sector." Our
contact person in this unit changes, but a good way to at least start the
process is to [visit the page for our geographic
area](https://uclouvain.be/fr/repertoires/entites/simm) (called SIMM, the "low
part of town" in LLN), and write the person listed there as "Responsable."

Standard computers (as of 2019) are Dell Latitude Windows machines. You can of
course install Linux on these (I've done so), and if you request one you can
also purchase a Macbook. Make sure to specify whether you want a QWERTY or an
AZERTY keyboard when you make your order!
