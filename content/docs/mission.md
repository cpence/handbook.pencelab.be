---
title: Mission
weight: 1
---

# Mission

## Research Goals

What exactly do we do here? It's a broad research group, with people interested
in a wide variety of different kinds of research projects. Broadly, though,
we're united by the following:

1. **A focus on the philosophy and history of science and technology.** All of
   our lab members are working on understanding the past and present of science
   and technology – with an emphasis on the life sciences. Contemporary life
   science engages a host of conceptual, philosophical, and ethical questions,
   all of which, to be fully understood, need to be contextualized within their
   history. That's what motivates us to get out of bed in the morning.
2. **An interdisciplinary approach to understanding.** Our group is committed to
   approaching these questions using tools from a variety of disciplines. A
   single project might involve analysis in the contemporary metaphysics of
   science or philosophy of biology, connections to the history of evolutionary
   theory, an analysis from the digital humanities, and worries about its
   ethical upshot in contemporary society.
3. **A collaborative outlook.** We won't be able to succeed without involving
   lots and lots of different people, from philosophers, to historians, digital
   humanists, and biologists. Most of our projects are multi-author, and many
   are multi-disciplinary or multi-institutional.

## Motivation

Why do we do what we do? Of course, each of us has our own motivation. But we
might generalize in the following way. Contemporary society gives real pride of
place to the sciences and scientific knowledge. The insights of "science" are
given particular weight in media and popular culture. This goes double for the
life sciences, which are taken to tell us not just how the natural world works,
but to tell us about _who we are._ If we are to take these pronouncements
seriously, we have to consider philosophical reflection about this knowledge.
How is it produced? What concepts does it employ? How are its theories
structured, and how do they relate to the natural world? How did they come to be
structured in that way? What is their impact on society?

## History of the Lab

Our group started out at Louisiana State University in the United States, in the
Department of Philosophy and Religious Studies, between 2014 and 2018. In 2018,
we moved to the Université catholique de Louvain in Belgium, housed in the
Institut supérieur de philosophie and the Centre de philosophie des sciences et
sociétés.
