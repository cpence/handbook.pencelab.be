---
title: Recurring Events and Tasks
weight: 5
---

# Recurring Events and Tasks

We have a number of required events that keep our culture alive and our work
moving forward productively. There are also some recurring tasks that are
assigned to lab members, which will be described here.

## CEFISES Seminars

Every one of you is also a member (automatically, by being affiliated with me)
of the Centre de philosophie des sciences et sociétés (CEFISES), and this comes
with a few obligations of its own. Most importantly, you need to attend the
weekly CEFISES seminar (currently on Friday afternoons). These meetings are a
series of research or work-in-progress talks, from local and remote scholars
(our group is often in charge of one, though the topics and responsibilities
change every year), in areas ranging from logic and rationality to general
philosophy of science to philosophy of a variety of special sciences (physics,
biology, cognitive science). They are a _fantastic_ way to keep yourself sharp
and benefit from the collective wisdom of the group.

## Individual Meetings

I do not currently have a practice of meeting periodically with every member of
the lab, one-on-one. (I may yet develop one.) I rely on postdoctoral fellows to
let me know when they'd like to chat, and on doctoral and master's students to
keep up a schedule of planned meetings and checkpoints that will propel them
forward on their work. To that end, if you're a postdoc and would like to meet,
_please ask_. If you're a master's or doctoral student, you should already know
when your next meeting will be, and what you will have to present there. If not,
_ask me_!

## Recurring Tasks

(Currently there are no lab-specific recurring tasks. Watch this space.)

You may also be asked to perform some recurring tasks for CEFISES. These
include:

- Importing and cataloging books in philosophy of science for the philosophy
  library.
- Updating events on the CEFISES website. This will require some quick training.
- Managing the CEFISES mailing lists (membership and moderation).
