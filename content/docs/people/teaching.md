---
title: Teaching
weight: 4
---

# Teaching

Postdoctoral fellows, and, to an occasional and much lesser extent, graduate
students, will have opportunities to teach courses while they're here. The
extent to which you can do so, however, is modulated by two factors.

First, if you don't know French, then you will be limited to teaching in those
classes in the master's program that we're willing to offer in English. These
are essentially limited to the master's-level philosophy of science course
(LFILO2602), which is offered in English every other year, and two courses at
the master's level that are usually offered to students in master's degrees in
the sciences (LSC2001 and LSC2220).

Second, in the vast majority of cases, there will not be money available to
designate you the official, sole instructor of record for classes – that
_requires,_ as a matter of university policy, that we pay you more money. That
said, you will be able to have hours "en suppléance" – essentially, as a
co-instructor partially replacing one of the faculty members in the center on
one of their classes. Depending on the arrangement, this could be near-complete
replacement with freedom to heavily modify the syllabus.

Further, if you'd like me to be able to really speak to your teaching abilities
in a recommendation letter, I'd be glad to arrange a teaching visit/evaluation,
where I visit one of your lectures and take enough notes to be able to write a
really fantastic teaching letter.
