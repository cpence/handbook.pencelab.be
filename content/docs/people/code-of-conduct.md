---
title: Code of Conduct
weight: 7
---

# Code of Conduct

I expect every member of the lab to be courteous, respectful, and professional,
not only in all of your interactions within the lab, but in any and all
professional conferences. Remember that you're representing our group, the
Institute, and the University when you interact with other scholars, staff,
administrators, etc.

In part, this involves treating others with basic human decency; whether or not
this is often lost on professional philosophers collectively. This is not the
place to shout or become agitated. I expect that all disputes will be resolved
calmly and patiently, with a cooling-off period if necessary.

In the first instance, should a resolution become impossible, I am the contact
to resolve interpersonal disputes -- you may come to me at any time, alone or in
a group. If my mediation fails, or if you feel that you cannot come to me for
whatever reason, your next contact is the President of the Institut supérieur de
philosophie (as your immediate boss for research purposes; currently Jean-Michel
Counet), or the head of the École de philosophie (as your immediate boss for
teaching purposes; currently Sylvain Camilleri). Should you not be able to
resolve the dispute in that context, they will be familiar with the appropriate
avenues for escalation.

Humor, of course, is a matter of culture. As you probably know, I have no
problem with joking around and swearing; I think we're all adult human beings
that need to interact with one another as such. That said, be sensitive to the
fact that people may not be, and in particular that "off-color" jokes (or jokes
that might be interpreted as sexist, racist, homophobic, etc.) are not
permissible in a work context.

Similarly, no behavior of a harassing nature will be tolerated, whether on the
basis of gender, sexual orientation, race, or any other category. Everyone has
the right to a productive, safe workspace.

Your central point-of-contact at the University for questions of harassment and
discrimination is the [Cellule Together
(FR).](https://uclouvain.be/fr/decouvrir/respect/cellule-d-aide-together.html)
They have also gathered all the relevant policies and procedures depending on
your status at the university and what kind of complaint is involved.
