---
title: Communication
weight: 2
---

# Communication

## Zulip

The short first commandment is: always use Zulip, whenever we can! Zulip is an
open-source alternative to programs like Slack and Teams, where we control all
of the data and run the server ourselves. It gives us an open, searchable
archive of all our communications, without having to worry about backups of
e-mails. Easy access to upload files and share comments, and varieties of group
and personal messaging, as well as a space for you to take notes.

There are apps for iOS and Android you can install, but you do not need them: you can just visit the site in your browser at <https://chat.pencelab.be/>.

Every new major lab project (grant, seminar, workshop, conference) should get
its own channel, and do your best to immediately invite anybody who is going to
be intimately involved in the project to the channel.

The e-mail policy which follows for post-doctoral fellows also holds for Zulip
channels.

## E-Mail Policy

Of course, you will have to check your e-mail regularly, because this is a
university. (In particular, do make sure to read the CEFISES mailing list
submissions whenever they go out! They're filled with useful information, talks,
etc.)

Following a policy I've heard about elsewhere (the KLI? Max Planck?), however, I
want to explicitly note that post-doctoral fellows **are not required** to check
their e-mail on Thursdays and Fridays, and may make this fact known to others.
You're here to do research, and I want to encourage you to have the time and
space to do so.

## Reminders

Of course, we're all amazingly busy people. That's what being an academic is
about. And part of working on collaborative projects is negotiating what kinds
of responsiveness to communication you can expect from others. But a few
guidelines are nice. As the PI, it's my goal to be available for comments and
assistance – [I say more about that over on the responsibilities page]({{<ref
"/docs/people/expectations.md">}}). In particular, I strive to be back to people
on anything important _within a week's time._

There will be times of year – other grant deadlines and grading season being the
two most obvious, though departmental service and administration tend to also
create similar black holes – when I'm liable to miss the mark. But importantly,
if you've waited a week and not heard anything, _feel free to nudge me about it_
and I can absolutely promise I will not hold it against you. While I try not to,
I absolutely do forget things, like anybody else!

I will of course not hold anyone _else_ to any particular communication
standard. But what I will demand is that everyone be _clear_ about when and to
what extent they are reachable, and that everyone _not_ take it out on someone
if they're working slower than you wanted, but still within the bounds of their
own declared standard operating procedure.

## Meetings

For the moment, we are reworking our own lab meeting schedules. (We used to have
a weekly paper reading group and monthly check-in meetings. They went away
during COVID, and it's not clear whether and how we're bringing them back.) For
postdoctoral fellows, I do _not_ in general schedule one-on-one meetings, except
for on demand. For doctoral and master's students, we will set up a meeting
calendar when you start work, and I expect you to stick to it.

In general, if a particular piece of work (whether a draft or even just reading
through some articles) will be required for a meeting, it will be made very
clear, at least a week in advance, that you need to have something prepared.
