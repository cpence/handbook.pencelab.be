---
title: Work Policies
weight: 3
---

# Work Policies

In general, the lab is an exceptionally relaxed place. Nobody's clocking in or
out, and I will never ask anyone for a time-sheet.

## Work Hours

As a general rule, I'm going to be in my office from Monday to Friday, from
around 9 or so until around 5 or so (and very often later into the evening). I
also have a pronounced tendency to work nights and weekends at home, especially
on low-concentration things like catching up on correspondence.

It's important to note that I _do not_ intend, in doing so, to imply that any
other lab members should need to work outside regular working hours. Except in
very rare circumstances (say, the submission deadlines of grants, grades, or
papers), which will be clearly communicated well in advance, I will never expect
a response from you on a night or weekend. Work-life balance is something that I
take very seriously, even if I'm often not particularly good at it myself. I
strongly encourage you to do as I say, not as I do.

## Working from Home

I have absolutely no problem with people working from home – I probably work
from home an average of one day every week or so. That said, one of the lab's
core values is to encourage collaboration and cooperation, and this doesn't work
very well if we never see you! So I do very much encourage lab members to
develop a schedule for being around, to the extent possible. The standard
_requirement_ for all members is three days per week -- if you regularly cannot
(or do not want to) meet this level of participation in the group, then this is
probably not the lab for you.

Of course, it's essential that you don't miss any of our [required regular
events]({{<ref "docs/people/events-tasks.md">}}) and are taking care of the
[regular expectations for lab members]({{<ref "docs/people/expectations.md">}}).

## Vacation

The number of vacation days that you're allotted is set by your contract; you
will be more familiar with it than I am, so I won't at all be checking in on
your number of vacation days. Often, projects that the lab is engaged in will
have a few events that are "unmissable" -- things like public talks,
conferences, or workshops -- and you should do everything you can not to have
these fall during vacation periods. Otherwise, as long as you give everyone a
bit of advance notice, you're not at all restricted regarding when you can take
vacations.
