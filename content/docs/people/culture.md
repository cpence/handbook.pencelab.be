---
title: Lab Culture
weight: 6
---

# Lab Culture

In case you can't tell, this is a pretty relaxed place. I don't even know _how_
to be a taskmaster. Our goal is to build a supportive, collaborative, human
working environment. I am firmly convinced that we can do great philosophy of
science, win all kinds of grants, and send all of you on to fantastic jobs
without causing anyone to stay awake for a week straight or need medical leave
for burnout.

To that end, some principles of the kind of lab I'd like us to be:

## 1. Sociable

We're academics – we like talking about our work with others. The biggest
advantage you have in coming to work in a group like ours is that _you're not
alone._ You'll have better ideas, with better arguments, in better contexts, if
you have people to work with and bounce those ideas off of.

So be sociable! Chat about your new paper ideas with others, head out for coffee
or drinks with the group, come along to seminars and think outside the box, etc.
It _will_ help your work, even more than taking two more hours to furiously type
in your office.

## 2. Cooperative

I'm a firm believer in the power and quality of coauthored, collaborative,
cooperative work. I firmly believe that contemporary philosophy of science
should drive us toward broader team-based work, and try to encourage this in our
group.

## 3. Nimble

Probably more as a result of my own proclivities than anything else, the lab
will often find itself working on a wide variety of projects at the same time.
We thus strongly encourage people to be quick on their feet, able to switch back
and forth between a number of tasks at once. And we're doing our best to build
the lab's tool and communication infrastructures to enable that sort of work.
