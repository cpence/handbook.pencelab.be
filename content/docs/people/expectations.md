---
title: Expectations
weight: 1
---

# Expectations

## Everyone

Before we start, this document is designed to lay out my expectations of lab
members. _Nothing_ here is beyond change! I'm always working to make us better,
and if you find something that seems wrong to you, let me know -- we can discuss
either individually or as a group.

Three expectations are demanded of absolutely everyone. First, every lab member
will adhere to the [code of conduct]({{<ref
"/docs/people/code-of-conduct.md">}}). Second, every lab member commits to being
present, on campus, for at least three days per week – otherwise, we're not a
lab, just a bunch of philosophers vaguely funded by the same people. And lastly,
everyone needs to be present at our various [required events]({{<ref
"/docs/people/events-tasks.md">}}).

## The PI

Hey! To the extent that most of this is written in the first person, it's
written by me, Charles. I'm the PI, a philosopher and historian of biology,
interested in evolutionary theory, digital humanities, and the ethics of
technology. (But... you probably already know that if you're reading this.)

My primary goal – in addition to doing my own research when and where I can, of
course – is to facilitate the production of a bunch of great work by a bunch of
great people. To that end, here's what you can expect from me:

- Prompt responses to commentary on drafts, project outlines, grant proposals.
  etc. I take this to be one of my most important job duties. We only all do
  great work if we're all working together – "rowing in the same direction," if
  you will.
- Any and all mentorship, help, and guidance that I can give. Like probably
  every professional academic, I've both benefited from exceptional mentorship
  at points in my career, and been burned by its absence. I want to make sure
  every lab member winds up on the right side of that.
- Related to the above, I will be available for spontaneous conversations. I am
  in my office quite a bit, and am almost _always_ happy to have a random chat
  about a project or a problem that you're having. (Don't be afraid to knock;
  I'll tell you if I can't talk!)
- It's my goal to set up every person who comes through the lab with their own
  projects that I'm happy to see them take along into the future. This is _not_
  a competitive environment. I want to be able to keep following all the awesome
  work that you will all go on to do after you leave!
- Lastly, I am deeply committed to being supportive on your projects in the
  manner that _you_ see fit. You can read more about this on [the authorship
  page]({{<ref "/docs/research/publishing/authorship.md">}}), but I both love
  coauthoring, and very much want you to leave with single-authored papers that
  future employers will look favorably on. Let me know how I can help you!

## Postdoctoral Fellows

Postdocs are the most independent members of the whole lab. While you will
usually be here on some project or other – whether of your own design or mine –
that's usually, to be quite frank, secondary to your constructing a really high
quality set of research and papers that will enable you to land an awesome job
after you leave. To that end, your primary goal should be to _do as much good
work as possible._

The only variable for postdocs is the amount and extent of teaching that you
would like to do while you're here. For that, see the [teaching page]({{<ref
"/docs/people/teaching.md">}}).

The expectations of postdoctoral fellows, then, are:

- Dedicated work on your research project. Remember that you're being funded by
  taxpayer money on the back of a _lot_ of work that's been put in by me and all
  of the other members of the lab, and we've gotten funding in order to secure
  some extremely awesome philosophical results. No pressure.
- A willingness to share your knowledge and collaborate. As a postdoc, you're
  not just here to learn things – you're here because you've already done
  excellent work, and the rest of us want to learn from you!
- Cooperation with demands of CEFISES and the ISP. You're far enough along in
  your career that you'll be expected to do a bit of service. Nothing too
  burdensome – if it ever is, tell me! – but enough to demonstrate some good
  will.

## Doctoral Students

Your job is to write your dissertation and leave, in a reasonable time frame
(something resembling four years, normally), and in the process to become a
professional scholar. This is simultaneously the easiest sentence in the world
to write and one of the most complicated things in the world to execute.

- Keep moving on your dissertation. Don't forget that your dissertation is the
  one thing standing between you and being done. It's cliché at this point, but
  _the best dissertation is a finished dissertation._
- You are in the process of becoming a professional scholar. To that end, you
  have to stop thinking like a student and start thinking like a pro. Start
  following journal contents alerts, thinking about preparing new projects and
  talks, and heading out to conferences to do networking. And let the other
  members of the lab help you with all of that! An excellent, and unfortunately
  common, way to fail at getting a PhD is just to think of it as "slightly
  harder normal coursework with a big final paper."
- You, also, will have a small amount of service work. Think of it as practice
  for your future career. But if it ever becomes too much, don't hesitate to ask
  for help – or a change of assignment.

## Master's Students

Your primary goal is to write your mémoire, and not to fall through the cracks.
Too many of our master's students take too long to get their degree, or even
fail to finish entirely.

- Plan meetings and writing, and stick to the plan! Your best way to get
  finished is to set obligations and stick to them.
- Ask for help when you need it. The lab is filled with people who can help you,
  if you introduce yourself and take the time to ask for help. Every single one
  of our doctoral students and postdocs would be glad to offer a helping hand,
  if they know something about your topic!
